#!/bin/bash
# Run like this: 
# 	curl https://gitlab.com/admiralcap/kk/-/raw/main/setup.sh | bash -s stmail01,stapp01 && tmux a
# OR
# 	curl https://gitlab.com/admiralcap/kk/-/raw/main/setup.sh | bash -s appservers && tmux a
 	
ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa -q -N ""

if [ "$1" = "appservers" ]; then
	host_ary=(stapp01 stapp02 stapp03)
else
	IFS=',' read -a host_ary <<< "$1"
fi

cat <<EOF > ~/.ssh/config
Host *
	StrictHostKeyChecking No
Host stapp01 stapp01.stratos.xfusioncorp.com
	HostName 172.16.238.10
	User tony
	#Password: Ir0nM@n
Host stapp02 stapp02.stratos.xfusioncorp.com
	HostName 172.16.238.11
	User steve
	#Password: Am3ric@
Host stapp03 stapp03.stratos.xfusioncorp.com
	HostName 172.16.238.12
	User banner
	#Password: BigGr33n
Host stlb01 stlb01.stratos.xfusioncorp.com
	HostName 172.16.238.14
	User loki
	#Password: Mischi3f
Host stdb01 stdb01.stratos.xfusioncorp.com
	HostName 172.16.239.10
	User peter
	#Password: Sp!dy
Host ststor01 ststor01.stratos.xfusioncorp.com
	HostName 172.16.238.15
	User natasha
	#Password: Bl@kW
Host stbkp01 stbkp01.stratos.xfusioncorp.com
	HostName 172.16.238.16
	User clint
	#Password: H@wk3y3
Host stmail01 stmail01.stratos.xfusioncorp.com
	HostName 172.16.238.17
	User groot
	#Password: Gr00T123
Host jump_host
	HostName jump_host.stratos.xfusioncorp.com
	User thor
	#Password: mjolnir123
Host jenkins jenkins.stratos.xfusioncorp.com
	HostName 172.16.238.19
	User jenkins
	#Password: j@rv!s
EOF


chmod 600 ~/.ssh/config
chmod 600 ~/.ssh/id_rsa.pub
chmod 400 ~/.ssh/id_rsa

echo mjolnir123 | sudo -S yum install tmux vim iproute telnet -y


host=${host_ary[0]}
password=$(grep -A3 "$host" ~/.ssh/config | grep 'Password:' | cut -d ' ' -f 2)
if [ -z "$password" ]; then
	echo "Unable to grep the password for host $host"
	exit 1
fi

tmux new -d "echo Password: $password; echo; ssh-copy-id $host; ssh $host 'echo $password > ~/.pw; cat .pw | sudo -S yum install vim iproute telnet -y'; ssh $host"

for (( i=1; i<${#host_ary[@]}; i++ )); do
	host=${host_ary[$i]}
	password=$(grep -A3 "$host" ~/.ssh/config | grep 'Password:' | cut -d ' ' -f 2)
	if [ -z "$password" ]; then
		echo "Unable to grep the password for host $host"
		echo Skipping!
		continue
	fi
	tmux splitw -h "echo Password: $password; ssh-copy-id $host; ssh $host 'echo $password > .pw; cat .pw | sudo -S yum install vim iproute telnet -y'; ssh $host"
done

tmux select-layout even-horizontal

echo "Run 'tmux a' if you haven't already."

